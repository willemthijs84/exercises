

<html>

<style>
    a {
        color:#000;
        display:block;
        font-size:80px;
        height:100px;
        position:relative;
        text-decoration:none;
        text-shadow:0 1px #fff;
        width:100px;
    }
    #chess_board { border:5px solid #333; }
    #chess_board td {
        background:#fff;
        background:-moz-linear-gradient(top, #fff, #eee);
        background:-webkit-gradient(linear,0 0, 0 100%, from(#fff), to(#eee));
        box-shadow:inset 0 0 0 1px #fff;
        -moz-box-shadow:inset 0 0 0 1px #fff;
        -webkit-box-shadow:inset 0 0 0 1px #fff;
        height:80px;
        text-align:center;
        vertical-align:middle;
        width:80px;
    }
    #chess_board tr:nth-child(odd) td:nth-child(even),
    #chess_board tr:nth-child(even) td:nth-child(odd) {
        background:#ccc;
        background:-moz-linear-gradient(top, #ccc, #eee);
        background:-webkit-gradient(linear,0 0, 0 100%, from(#ccc), to(#eee));
        box-shadow:inset 0 0 10px rgba(0,0,0,.4);
        -moz-box-shadow:inset 0 0 10px rgba(0,0,0,.4);
        -webkit-box-shadow:inset 0 0 10px rgba(0,0,0,.4);
    }


</style>

<body>


<table id="chess_board" cellpadding="0" cellspacing="0">
<?php

$blackpawns = array('&#9823;','&#9820;','&#9822;','&#9821;','&#9819;','&#9818;','&#9821;','&#9822;','&#9820;');
$whitepawns = array('&#9817;','&#9814;','&#9816;','&#9815;','&#9813;','&#9812;','&#9815;','&#9816;','&#9814;');


$teller = 1;
for($i=1; $i<=64; $i++)
{
    $pawn = '';
    if($i<9)
    $pawn = $blackpawns[$i];

    if($i>8&&$i<17)
    $pawn = $blackpawns[0];

    if($i>48&&$i<57)
    $pawn = $whitepawns[0];

    if($i>56)
    $pawn = $whitepawns[$i-56];

    if($teller==1){ echo '<tr>'; }
        echo '<td><a href="#">'.$pawn.'</a></td>';
    if($teller==8){ $teller=0; echo '</tr>'; }

    $teller++;


}

?>

</table>


</body>


</html>