# Recursive function #

Een recursieve functie is een functie die naar zichzelf verwijst.

bvb

    function fact($n) {
      if ($n === 0) { // our base case
         return 1;
      }
      else {
         return $n * fact($n-1); // <--calling itself.
      }
    }


Vaak is recursie een natuurlijke en elegante manier om functies of procedures te definiëren. In een daadwerkelijke implementatie moet men er echter voorzichtig mee zijn. Hoewel recursie soms snel en efficiënt werkt, is het ook vaak veel trager dan niet-recursieve implementaties.

# Value object #

Een Value Object is een klein object dat een eenvoudige entiteit vertegenwoordigd, waarvan de gelijkheid niet gebaseerd is op de identiteit.
Twee value objects zijn gelijk als ze dezelfde waarde hebben, niet noodzakelijkerwijs hetzelfde object.

    address1 = new Address('Main street 42')
    address2 = new Address('Main street 42')

    address1 == address2  // true

Omdat de waarden gelijk zijn, worden beide objecten als gelijk beschouwd, zelfs al zijn ze verschillende instanties.

PHP heeft geen afzonderlijke begrip "value objecten", PHP heeft slechts één type object. De vergelijking operator kan dat onderscheid wel te maken:

Bij de vergelijking operator (==) worden object-ariabelen vergeleken op eenvoudige wijze, namelijk twee objectinstanties zijn gelijk als ze dezelfde attributen en waarden hebben en instanties zijn van dezelfde klasse.

Bij gebruik van de identiteit operator (===), zijn object-variabelen slechts identiek als ze verwijzen naar hetzelfde instantie van dezelfde klasse.

    $o = new Flag();
    $p = new Flag();
    $q = $o;
    $r = new OtherFlag();

    Two instances of the same class
    $o == $q : TRUE
    $o != $q : FALSE
    $o === $q : FALSE
    $o !== $q : TRUE

    Two references to the same instance
    $o == o2 : TRUE
    $o != o2 : FALSE
    $o === o2 : TRUE
    $o !== o2 : FALSE

    Instances of two different classes
    $o == $r : FALSE
    $o != $r : TRUE
    $o === $r : FALSE
    $o !== $r : TRUE

# function format_plural($count, $singular, $plural, array $args = array(), array $options = array()) #

Deze functie zorgt ervoor dat meervoud versie van een string correct verloopt. Hierin mogen geen reeds gelokaliseerde strings in meegegeven worden, aangezien de t() functie wordt gebruikt door deze functie.

voorbeeld:

    $output = format_plural($node->comment_count, '1 comment', ''@count' comments');

voorbeeld met optionele parameters:

    $output = format_plural($update_count,
        'Changed the content type of 1 post from %old-type to %new-type.',
        'Changed the content type of @count posts from %old-type to %new-type.',
        array('%old-type' => $info->old_type, '%new-type' => $info->new_type));

## Parameters ##

$count: The item count to display.

$singular: The string for the singular case. Make sure it is clear this is singular, to ease translation (e.g. use "1 new comment" instead of "1 new"). Do not use @ count in the singular string.

$plural: The string for the plural case. Make sure it is clear this is plural, to ease translation. Use @ count in place of the item count, as in "@ count new comments".

$args: An associative array of replacements to make after translation. Instances of any key in this array are replaced with the corresponding value. Based on the first character of the key, the value is escaped and/or themed. See format_string(). Note that you do not need to include @ count in this array; this replacement is done automatically for the plural case.

$options: An associative array of additional options. See t() for allowed keys.

Return value

A translated string.

# view meest bekeken producten

https://www.drupal.org/project/commerce_productpopularity
https://www.drupal.org/project/1643068

https://www.drupal.org/documentation/modules/statistics


# producten vergelijken (hoe aanpakken, welke modules)

https://www.drupal.org/project/commerce_product_comparison
https://www.drupal.org/project/commerce_product_comparison

# What is acceptance testing and how would you automate it?

Een acceptatietest is een test om iets wel of niet te accepteren.

In voorafgaande testen zoals de unittest, programmatest, functietest, integratietest, systeemtest, regressietest, schaduwtest, is (na eventuele aanpassing) vastgesteld dat het product of de dienst in een voldoende mate voldoet aan de requirements. Centrale vraag bij deze eerste testen was "doet het systeem het zoals gevraagd"?

Bij de acceptatietest wordt nagegaan of er daarnaast problemen zijn te verwachten in het gebruik die eerder nog niet gevonden zijn.

Bij het formuleren van de requirements kunnen er bijvoorbeeld ook fouten gemaakt zijn. Ze kunnen bijvoorbeeld onvolledig zijn, of onduidelijk geformuleerd. Een systeem kan volledig aan de requirements voldoen, maar toch niet acceptabel zijn.

Automatiseren door bvb codeception toe te passen, maar ook door OTAP toe te passen:

Het Nederlandse begrip is afgeleid van het Engelse DTAP: Development, Testing, Acceptance and Production.
http://nl.wikipedia.org/wiki/OTAP

# SOLID

SOLID is een ezelsbruggetje dat staat voor de 5 basisprincipes van object-geörienteerde programatie en design

S - Single responsibility principle

Een classe mag slechts een verantwoordelijkheid hebben.
D.w.z dat slechts één potentiële weiziging in de software de specificatie van de klasse kan beïnvloeden.

O - Open/closed principle

Software entities zouden open moeten staan voor extensies, maar gesloten zijn voor aanpassingen.

L - Liskov substitution principle

Objecten zouden vervangen kunnen worden met andere instanties van hetzelfde type zonder dat deze de werking van het programma hindert.

I - Interface segregation principle

Vele klantspecifieke interfaces zijn beter dan een algemene interface.

D - Dependency inversion principle

one should “Depend upon Abstractions. Do not depend upon concretions.”

Elke afhankelijkheid in het ontwerp moet zich richten op een interface of een abstracte klasse.
Niet op een concrete klasse.

http://en.wikipedia.org/wiki/SOLID_(object-oriented_design)#cite_note-martin-design-principles-8